Machine à câlin
===============

Évoque, convoque les 5 sens
---------------------------

- toucher douceur (chaleur ?)
- image (la machine a une forme et elle bouge!)
- son  appel puis berceuse ou sons ambiance
- odeur ? goût ?

ordre:
------
- Animal / humain    OU   objet affectif nounours, ou boudha
- Tendresse, évoque « la main » les fanfreluches????

Technique
---------

* MOTEURS
    * 2 lèves-vitre de récup auto (XM ou scénic)
	* 4 fins-de-course (2 bas ; 2 haut)
    * moteur pour faire tourner la boite à musique
* CAPTEURS
    * présence « in »  pour départ de cycle
    * anti-panique
    * présence « devant » lance « appel »

Alimentation
------------
* 2 alim, de PC (récup)
* Étage de puissance
* Électronique centrale : Arduino

Programme
---------
* 1/ Initialisation, les moteurs commandant les bras vont chercher leurs fin de course, puis, mise en position « bras ouverts ».
* 2/ capteur « présence » déclenche cycle

![concept](photos/maca.jpg)
![](photos/MAC_221.JPG)
![](photos/MAC_233.JPG)