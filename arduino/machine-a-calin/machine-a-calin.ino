/*
 * Machine a calins
 * 
 * Moteurs bras gauche et droit
 * Moteur pas a pas boite a musique
 * 
 * Fins de courses bras ouverts + bras fermes
 * Capteur de presence personne a clianer (ultra son)
 * Bouton panique
 */

//------------ bras
#define BRAS2_PWM 11 // green
#define BRAS2_DIR 13 // yellow
#define BRAS1_PWM 10 // orange
#define BRAS1_DIR 12 // red

//------------ boite a musique (axe X de la CNC shield)
// The following is a simple stepper motor control procedures
#define EN 8 // stepper motor enable , active low
#define Y_DIR 6 // Y-axis stepper motor direction control
#define Y_STP 3 // Y-axis stepper control
#define STEP 400
#define MICRO_DELAY 1500

//------------- capteurs
#define BRAS1_OPEN 9
#define BRAS1_CLOSED 7  
#define BRAS2_OPEN 5
#define BRAS2_CLOSED 4
#define CAPTEUR_DISTANCE_ECHO 2
#define CAPTEUR_DISTANCE_TRIG A0
#define BTN_PANIQUE A1
#define CAPTEUR_BOITE_MUSIQUE A2
#define LED A3
#define BTN_BAM_MANU A4

#define BRAS1_SERRE LOW
#define BRAS1_DESERRE HIGH
#define BRAS2_SERRE LOW
#define BRAS2_DESERRE HIGH

//#define MARGE_DISTANCE_PRESENCE 15*58   // 15cm
//#define MARGE_DISTANCE_PANIQUE  10*58   // 10cm
//
//#define NB_ROTATION 10   // 30 tours de moteurs = 1 cycle boite a musique

#define DIST_PRESENCE 110*58
#define DIST_PANIQUE 105*58

#define DELAY_ENTRE_ETATS 1000
#define DELAY_ENTRE_CYCLES 5000
#define DELAY_FIN_SETUP 10000
#define DELAY_ENTRE_CYCLES_BAM 24700
#define DELAY_DEMARRAGE 2000

enum MACState {
  MAC_ATTENTE,    // 0
  MAC_SERRAGE_1,  // 1
  MAC_SERRAGE_2,  // 2
  MAC_CALIN,      // 3
  MAC_RELACHE_1,  // 4
  MAC_RELACHE_2,  // 5
  MAC_PANIQUE     // 6
};


//------------- declaration des fonctions
void initDistUtilisateur();
String nomEtat(MACState);
void stopTousMoteurs();



//-------------- variables 
int motor_speed = 100; // PWM 0 to 255 vitesse ouverture/fermeture bras
int motor_fast_speed = 200;
long distance_sans_utilisateur;
long distance_utilisateur;
int step_rotation;
long debut_cycle;

long debut_etat[7] = {-1, -1, -1, -1, -1, -1, -1};
long timeout_etat[7] = {-1, 10000, 10000, 40000, 5000, 5000, -1};

MACState etat_courant = MAC_ATTENTE;
MACState etat_precedent = MAC_ATTENTE;



void setup() {
  Serial.begin(115200);

  // definition des entrees/sorties
  //--- boite a musique
  pinMode(Y_DIR, OUTPUT);
  pinMode(Y_STP, OUTPUT);
  pinMode(EN, OUTPUT);

  //---- moteurs bras
  pinMode(BRAS1_PWM, OUTPUT);
  pinMode(BRAS2_PWM, OUTPUT);
  pinMode(BRAS1_DIR, OUTPUT);
  pinMode(BRAS2_DIR, OUTPUT);

  //------ fins de course
  pinMode(BRAS1_OPEN, INPUT_PULLUP);
  pinMode(BRAS2_OPEN, INPUT_PULLUP);
  pinMode(BRAS1_CLOSED, INPUT_PULLUP);
  pinMode(BRAS2_CLOSED, INPUT_PULLUP);

  //------- bouton panique
  pinMode(BTN_PANIQUE, INPUT_PULLUP);

  //------- bouton entre reset et panic inutilise...
  pinMode(BTN_BAM_MANU, INPUT_PULLUP);
    

  //------- capteur distance utilisateur
  pinMode(CAPTEUR_DISTANCE_TRIG, OUTPUT);
  pinMode(CAPTEUR_DISTANCE_ECHO, INPUT);

  //-------- fourche optique boite a musique
  pinMode(CAPTEUR_BOITE_MUSIQUE, INPUT);

  //-------- LED 
  pinMode(LED, OUTPUT);

  // etat initial des sorties
  //-- desactivation boite a musique
  digitalWrite(EN, HIGH);

      
  // 1 : initialisation capteur de distance
  initDistUtilisateur();

  // 1bis lecture distance tant que pas d'appui du bouton panique
  /*
  while(digitalRead(BTN_PANIQUE)) {
    digitalWrite(LED, !digitalRead(LED));
    initDistUtilisateur();
  }
  digitalWrite(LED, HIGH);
  */

  // 1ter : verif des contacts
  if ((!digitalRead(BRAS1_CLOSED) && !digitalRead(BRAS1_OPEN)) ||
    (!digitalRead(BRAS2_CLOSED) && !digitalRead(BRAS2_OPEN))) {
      Serial.println("--------- ETATS CAPTEURS --------");
      delay(1000);
      while(! (!digitalRead(BRAS1_CLOSED) && !digitalRead(BRAS1_OPEN)) &&
        !(!digitalRead(BRAS2_CLOSED) && !digitalRead(BRAS2_OPEN))) {
          Serial.print("bras1 f:");
          Serial.print(digitalRead(BRAS1_CLOSED));
          Serial.print(", o: ");
          Serial.println(digitalRead(BRAS1_OPEN));
          Serial.print("bras2 f:");
          Serial.print(digitalRead(BRAS2_CLOSED));
          Serial.print(", o: ");
          Serial.println(digitalRead(BRAS2_OPEN));
          Serial.print("fin cycle bam :");
          Serial.println(digitalRead(CAPTEUR_BOITE_MUSIQUE));
          Serial.print("panic :");
          Serial.println(digitalRead(BTN_PANIQUE));

          initDistUtilisateur();
  
          delay(1000);
      }
  }

  // 2 : courses completes des moteurs de bras et determination des sens des moteurs
  // serrage du bras1
  delay(5000);
  if (1) {
  Serial.println("fermeture bras 1");
  digitalWrite(BRAS1_DIR, BRAS1_SERRE);
  while (digitalRead(BRAS1_CLOSED)) {
    analogWrite(BRAS1_PWM, motor_speed);
    delay(500);
  }
  analogWrite(BRAS1_PWM, 0);
  delay(DELAY_ENTRE_ETATS);
  // serrage du bras2
  Serial.println("fermeture bras 2");
  digitalWrite(BRAS2_DIR, BRAS2_SERRE);
  while (digitalRead(BRAS2_CLOSED)) {
    analogWrite(BRAS2_PWM, motor_speed);
    delay(500);
  }
  analogWrite(BRAS2_PWM, 0);
  delay(DELAY_ENTRE_ETATS);
  // deserrage du bras2
  Serial.println("ouverture bras 2");
  digitalWrite(BRAS2_DIR, BRAS2_DESERRE);
  while (digitalRead(BRAS2_OPEN)) {
    analogWrite(BRAS2_PWM, motor_speed);
    delay(500);
  }
  analogWrite(BRAS2_PWM, 0);
  delay(DELAY_ENTRE_ETATS);
  // deserrage du bras1
  Serial.println("ouverture bras 1");
  digitalWrite(BRAS1_DIR, BRAS1_DESERRE);
  while (digitalRead(BRAS1_OPEN)) {
    analogWrite(BRAS1_PWM, motor_speed);
    delay(500);
  }
  analogWrite(BRAS1_PWM, 0);
  delay(DELAY_ENTRE_ETATS);
  }

  // 3 : cycle boite a musique
  /*
  Serial.println("boite a musique");
  digitalWrite(EN, LOW);
  digitalWrite(Y_DIR, LOW); // Enables the motor to move in a particular direction
  analogWrite(Y_STP, 128);
  delay(DELAY_ENTRE_CYCLES_BAM );
  //while (!digitalRead(CAPTEUR_BOITE_MUSIQUE));
  digitalWrite(EN, HIGH);
  analogWrite(Y_STP, 0);
  */

  Serial.println("fin setup");
  delay(DELAY_FIN_SETUP);
}

void initDistUtilisateur() {
  // 1ere mesure "a vide" sans tenir compte du resultat
  digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
  delay(750);

  // moyenne sur 3 mesures
  digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
  distance_sans_utilisateur = pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
  delay(750);

  digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
  distance_sans_utilisateur += pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
  delay(750);

  digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
  distance_sans_utilisateur += pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
  delay(750);

  distance_sans_utilisateur /= 3.0;
  
  Serial.print("Distance a vide en cm : "); 
  Serial.println(distance_sans_utilisateur / 58);
}

void loop() {
  if (!digitalRead(BTN_PANIQUE)) {
    Serial.println("Bouton PANIQUE !");
    etat_precedent = etat_courant;
    etat_courant = MAC_PANIQUE; 
  }

  if (etat_courant != MAC_PANIQUE) {
    // mesure distance, utile dans tous les cas
    digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
    long distance = pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
    //Serial.println(distance / 58);

    if(etat_courant == MAC_ATTENTE) {
      digitalWrite(LED, LOW);
      //if (distance <= distance_sans_utilisateur - MARGE_DISTANCE_PRESENCE) {
      //if (distance > DIST_PANIQUE && distance <= DIST_PRESENCE) {
      if (distance <= DIST_PRESENCE) {
        // on valide en faisant 2 autres mesures
        delay(250);
        digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
        delayMicroseconds(10);
        digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
        distance = pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
        if (distance <= DIST_PRESENCE) {
          // on valide en faisant 2 autres mesures
          delay(250);
          digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
          delayMicroseconds(10);
          digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
          distance = pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
          if (distance <= DIST_PRESENCE) {
            distance_utilisateur = distance;
            etat_precedent = etat_courant;
            etat_courant = MAC_SERRAGE_1;
            delay(DELAY_DEMARRAGE);
          }
        }
      }
    } else {
      digitalWrite(LED, HIGH);
      //if (distance <= distance_utilisateur - MARGE_DISTANCE_PANIQUE) {
      /*
      if (distance <= DIST_PANIQUE) {
        Serial.print("Panic distance ? ");
        Serial.print(distance / 58);
        Serial.print(" vs ");
        Serial.println(distance_utilisateur / 58);
        
        // check via 2eme mesure
        digitalWrite(CAPTEUR_DISTANCE_TRIG, HIGH);
        delayMicroseconds(10);
        digitalWrite(CAPTEUR_DISTANCE_TRIG, LOW);
        distance = pulseIn(CAPTEUR_DISTANCE_ECHO, HIGH);
        Serial.print("Panic distance check ");
        Serial.print(distance / 58);
        Serial.print(" vs ");
        Serial.println(distance_utilisateur / 58);

        //if (distance <= distance_utilisateur - MARGE_DISTANCE_PANIQUE) {
        if (distance <= DIST_PANIQUE) {
          etat_precedent = etat_courant;
          etat_courant = MAC_PANIQUE;
        }
      }
      */
    }
  }

  if (etat_courant != etat_precedent) {
    debut_etat[etat_courant] = millis();
    Serial.print("etat : ");
    Serial.print(nomEtat(etat_precedent));
    Serial.print(" --> ");
    Serial.println(nomEtat(etat_courant));
  }

  // check timeout
  if(timeout_etat[etat_courant] > 0 &&
    millis() - debut_etat[etat_courant] > timeout_etat[etat_courant]) {
      Serial.println("Timeout depasse, on arrete tout !");
    stopTousMoteurs();
  }
  
  switch(etat_courant) {
    case  MAC_ATTENTE:
      etat_precedent = MAC_ATTENTE;
      // boite a musique en mode manuel pour recalage
      if(!digitalRead(BTN_BAM_MANU)) {
        digitalWrite(EN, LOW);
        analogWrite(Y_STP, 128); 
      }
      while(!digitalRead(BTN_BAM_MANU));
      digitalWrite(EN, HIGH);
      analogWrite(Y_STP, 0);
      break;
    case MAC_SERRAGE_1:
      digitalWrite(BRAS1_DIR, BRAS1_SERRE);
      if (digitalRead(BRAS1_CLOSED)) {
        etat_precedent = MAC_SERRAGE_1;
        analogWrite(BRAS1_PWM, motor_speed);
        delay(100);
      } else {
        analogWrite(BRAS1_PWM, 0);
        etat_precedent = MAC_SERRAGE_1;
        etat_courant = MAC_SERRAGE_2;
        delay(DELAY_ENTRE_ETATS);
      }
      break;
    case MAC_SERRAGE_2:
      digitalWrite(BRAS2_DIR, BRAS2_SERRE);
      if (digitalRead(BRAS2_CLOSED)) {
        etat_precedent = MAC_SERRAGE_2;
        analogWrite(BRAS2_PWM, motor_speed);
        delay(100);
      } else {
        analogWrite(BRAS2_PWM, 0);
        etat_precedent = MAC_SERRAGE_2;
        etat_courant = MAC_CALIN;
        delay(DELAY_ENTRE_ETATS);
      }
      break;
    case MAC_CALIN:
      if (etat_precedent != MAC_CALIN) {
        debut_cycle = millis();
        step_rotation = 0;
        digitalWrite(EN, LOW);
        
      }
      etat_precedent = MAC_CALIN;
      if (millis() > debut_cycle + DELAY_ENTRE_CYCLES_BAM 
          /*&& !digitalRead(CAPTEUR_BOITE_MUSIQUE)*/) {
        digitalWrite(EN, LOW);
        digitalWrite(Y_DIR, LOW);
        analogWrite(Y_STP, 0);
        etat_precedent = MAC_CALIN;
        etat_courant = MAC_RELACHE_2;
        delay(DELAY_ENTRE_ETATS);
      } else {
        analogWrite(Y_STP, 128);
      }
      break;
    case MAC_RELACHE_1:
      digitalWrite(BRAS1_DIR, BRAS1_DESERRE);
      if (digitalRead(BRAS1_OPEN)) {
        etat_precedent = MAC_RELACHE_1;
        analogWrite(BRAS1_PWM, motor_fast_speed);
        delay(50);
      } else {
        analogWrite(BRAS1_PWM, 0);
        etat_precedent = MAC_RELACHE_1;
        etat_courant = MAC_ATTENTE;
        delay(DELAY_ENTRE_ETATS);
        delay(DELAY_ENTRE_CYCLES);
      }
      break;
    case MAC_RELACHE_2:
      digitalWrite(BRAS2_DIR, BRAS2_DESERRE);
      if (digitalRead(BRAS2_OPEN)) {
        etat_precedent = MAC_RELACHE_2;
        analogWrite(BRAS2_PWM, motor_fast_speed);
        delay(50);
      } else {
        analogWrite(BRAS2_PWM, 0);
        etat_precedent = MAC_RELACHE_2;
        etat_courant = MAC_RELACHE_1;
        delay(DELAY_ENTRE_ETATS);
      }
      break;
    case MAC_PANIQUE:
      digitalWrite(EN, HIGH);
      if ((etat_precedent == MAC_SERRAGE_1 || etat_precedent == MAC_RELACHE_1) && (digitalRead(BRAS2_OPEN))) {
        digitalWrite(BRAS1_DIR, BRAS1_DESERRE);
        while (digitalRead(BRAS1_OPEN)) {
          analogWrite(BRAS1_PWM, motor_fast_speed);
          delay(100);
        }
        analogWrite(BRAS1_PWM, 0);
      } else {
        digitalWrite(BRAS2_DIR, BRAS2_DESERRE);
        while (digitalRead(BRAS2_OPEN)) {
          analogWrite(BRAS2_PWM, motor_fast_speed);
          delay(100);
        }
        analogWrite(BRAS2_PWM, 0);
        digitalWrite(BRAS1_DIR, BRAS1_DESERRE);
        while (digitalRead(BRAS1_OPEN)) {
          analogWrite(BRAS1_PWM, motor_fast_speed);
          delay(100);
        }
        analogWrite(BRAS1_PWM, 0);
      }
      delay(DELAY_ENTRE_CYCLES);
      etat_courant = MAC_ATTENTE;
      etat_precedent = MAC_PANIQUE;
      break;
  }
}

void stopTousMoteurs() {
  // bras1
  analogWrite(BRAS1_PWM, 0);
  // bras2
  analogWrite(BRAS2_PWM, 0);
  // boite a musique
  digitalWrite(EN, HIGH);
  analogWrite(Y_STP, 0);

  // boucle infine, on attend un reset complet...
  while(true) {
    digitalWrite(LED, HIGH);
    delay(2000);
    for (int i=0; i<3; i++) {
      digitalWrite(LED, LOW);
      delay(300);
      digitalWrite(LED, HIGH);
      delay(300);
    }
    for (int i=0; i<3; i++) {
      digitalWrite(LED, LOW);
      delay(800);
      digitalWrite(LED, HIGH);
      delay(300);
    }
    for (int i=0; i<3; i++) {
      digitalWrite(LED, LOW);
      delay(300);
      digitalWrite(LED, HIGH);
      delay(300);
    }

  }
}

String nomEtat(MACState state) {
  switch(state) {
    case MAC_ATTENTE:
      return "MAC_ATTENTE";
      break;
    case MAC_SERRAGE_1:
      return "MAC_SERRAGE_1";
      break;
    case MAC_SERRAGE_2:
      return "MAC_SERRAGE_2";
      break;
    case MAC_CALIN:
      return "MAC_CALIN";
      break;
    case MAC_RELACHE_1:
      return "MAC_RELACHE_1";
      break;
    case MAC_RELACHE_2:
      return "MAC_RELACHE_2";
      break;
    case MAC_PANIQUE:
      return "MAC_PANIQUE";
      break;
    default:
      return "INCONNU";  
  }
}

