// The following is a simple stepper motor control procedures
#define EN 8 // stepper motor enable , active low
#define ENZ 1 // stepper motor enable , active low
#define X_DIR 5 // X -axis stepper motor direction control
#define Y_DIR 6 // y -axis stepper motor direction control
#define Z_DIR 7 // z axis stepper motor direction control
#define A_DIR 12 // A axis stepper motor direction control
#define X_STP 2 // x -axis stepper control
#define Y_STP 3 // y -axis stepper control
#define Z_STP 4 // z -axis stepper control
#define A_STP 13 // z -axis stepper control
#define STEP 400
#define MICRO_DELAY 900

int NB_ROTATION = 2;

void setup () {   // The stepper motor used in the IO pin is set to output
 pinMode (X_DIR, OUTPUT);
 pinMode (X_STP, OUTPUT);
 pinMode (Y_DIR, OUTPUT);
 pinMode (Y_STP, OUTPUT);
 pinMode (Z_DIR, OUTPUT);
 pinMode (Z_STP, OUTPUT);
 pinMode (A_DIR, OUTPUT);
 pinMode (A_STP, OUTPUT);
 pinMode (EN, OUTPUT);
 digitalWrite (EN, LOW);
 pinMode (ENZ, OUTPUT);
 digitalWrite (ENZ, LOW);
}

void loop () {
  digitalWrite(X_DIR, HIGH); // Enables the motor to move in a particular direction
  // Makes STEP pulses for making one full cycle rotation
  for(int x = 0; x < STEP * NB_ROTATION; x++) {
    digitalWrite(X_STP, HIGH); 
    delayMicroseconds(MICRO_DELAY);
    digitalWrite(X_STP, LOW); 
    delayMicroseconds(MICRO_DELAY);
  }
  delay(1000);
}
