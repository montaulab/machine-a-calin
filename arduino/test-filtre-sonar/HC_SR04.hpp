#ifndef HC_SR04_HPP
#define HC_SR04_HPP

class HC_SR04
{
  /* Sound speed in mm/us */
  static constexpr float SOUND_SPEED = 343.0 / 1000.0;
  
public:
  HC_SR04( byte triggerPin, byte echoPin, unsigned long timeout ):
    mTriggerPin( triggerPin ),
    mEchoPin( echoPin ),
    mTimeout( timeout )
  {
    pinMode( mTriggerPin, OUTPUT );
    digitalWrite( mTriggerPin, LOW );
    pinMode( mEchoPin, INPUT );
  }
  
  ~HC_SR04() {};

  bool probe( float & distance )
  {
    digitalWrite( mTriggerPin, HIGH );
    delayMicroseconds( 10 );
    digitalWrite( mTriggerPin, LOW );

    /* get echo duration divide by 2 ( return path ) */
    long duration = pulseIn( mEchoPin, HIGH, mTimeout ) / 2.0;

    if( 0 == duration ) return false;
    
    /* Compute distance = time * speed */
    distance = duration * SOUND_SPEED;
    
    return true;
  }

protected:
  byte mTriggerPin;
  byte mEchoPin;
  unsigned long mTimeout;
  
};

#endif // HC_SR04_HPP
