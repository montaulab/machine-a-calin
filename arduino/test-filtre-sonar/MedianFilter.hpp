#ifndef MEDIANFILTER_HPP
#define MEDIANFILTER_HPP

#include <stdint.h>
#include <stdlib.h>

template< typename T, unsigned int N >
class MedianFilter
{
public:
  MedianFilter()
  {
    for( uint8_t i = 0; i < N; ++i )
    {
      this->mPointers[ i ] = & this->mValues[ i ];
    }
  }

  void update( const T & newValue )
  {
    if( this->mValid < N ) ++this->mValid;
    this->mValues[ this->mIndex ] = newValue;
    ++this->mIndex;
    if( this->mIndex >= N ) this->mIndex = 0;

    qsort( this->mPointers, this->mValid, sizeof( T * ), MedianFilter::compare );

    this->mMedian = * mPointers[ this->mValid / 2 ];
  }

  inline const T & get() const
  {
    return this->mMedian;
  }
  
protected:
  static int compare( const void * lhs, const void * rhs )
  {
    if( ( * static_cast< const T * const * >( lhs ) ) > ( * static_cast< const T * const * >( rhs ) ) ) return 1;
    return -1;
  }

private:
  uint8_t mIndex = 0;
  uint8_t mValid = 0;
  T mValues[ N ];
  T * mPointers[ N ];
  T mMedian = 0;
};

#endif
