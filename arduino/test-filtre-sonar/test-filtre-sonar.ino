#include "AverageFilter.hpp"
#include "MedianFilter.hpp"
#include "HC_SR04.hpp"

#define CAPTEUR_DISTANCE_ECHO 2
#define CAPTEUR_DISTANCE_TRIG A0

AverageFilter< float, 64 > filtre1;
AverageFilter< float, 64 > filtre4;
MedianFilter<  float, 64 > filtre3;

/* Constantes pour le timeout */
const unsigned long TIMEOUT = 25000UL; // 25ms = ~8m

HC_SR04 sonar( CAPTEUR_DISTANCE_TRIG, CAPTEUR_DISTANCE_ECHO, TIMEOUT );

void setup() {
  Serial.begin( 115200 );
}

void loop() {

  float distance = 0.0;
  delay( 20 );
  if( sonar.probe( distance ) )
  {
    filtre1.update( distance );
//    filtre2.update( distance );
    filtre3.update( filtre1.get() );
    filtre4.update( filtre3.get() );
//    Serial.print( static_cast<long>( distance ) );
//    Serial.print(','); 
//    Serial.print( static_cast<long>( filtre1.get() ) );
//    Serial.print(','); 
//    Serial.print( static_cast<long>( filtre2.get() ) );
//    Serial.print(','); 
//    Serial.print( static_cast<long>( filtre3.get() ) );
//    Serial.print(','); 
    Serial.print( static_cast<long>( filtre4.get() ) );
    Serial.print(",0,50");
    Serial.print('\n');
  }
}
